import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Request,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Pagination } from 'nestjs-typeorm-paginate';
import { catchError, map, Observable, of, tap } from 'rxjs';
import { hasRole } from 'src/auth/decorator/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guard/jwt-guard';
import { RoleGuard } from 'src/auth/guard/role.guard';
import { IUser, UserRole } from '../models/user.interface';
import { UserService } from '../service/user.service';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import path = require('path');
import { join } from 'path';
import { UserIsUser } from 'src/auth/guard/UserIsUser.guard';

export const storage = {
  storage: diskStorage({
    destination: './uploads/profileimages',
    filename: (req, file, cb) => {
      const filename: string =
        path.parse(file.originalname).name.replace(/\s/g, '') + uuidv4();
      const extension: string = path.parse(file.originalname).ext;

      cb(null, `${filename}${extension}`);
    },
  }),
};

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  create(@Body() user: IUser): Observable<IUser | any> {
    return this.userService.createUser(user).pipe(
      map(
        (user: IUser) => user,
        catchError((err) => of({ error: err.message })),
      ),
    );
  }

  @Post('login')
  login(@Body() user: IUser): Observable<any> {
    return this.userService.login(user).pipe(
      map((jwt: string) => {
        return { access_token: jwt };
      }),
    );
  }

  @Get(':id')
  findOne(@Param() params): Observable<IUser> {
    return this.userService.findOne(params.id);
  }

  // @hasRole(UserRole.ADMIN)
  // @UseGuards(JwtAuthGuard, RoleGuard)
  @Get()
  index(
    @Query('page') page = 1,
    @Query('limit') limit = 10,
    @Query('username') username: string,
  ): Observable<Pagination<IUser>> {
    limit = limit > 100 ? 100 : limit;
    if (username === undefined || username === null) {
      return this.userService.paginate({
        page: Number(page),
        limit: Number(limit),
        route: 'https://blog-nestjs.herokuapp.com/user',
      });
    } else {
      return this.userService.paginateFilterByUsername(
        {
          page: Number(page),
          limit: Number(limit),
          route: 'https://blog-nestjs.herokuapp.com/user',
        },
        { username },
      );
    }
  }

  @hasRole(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Delete(':id')
  deleteOne(@Param('id') param: string): Observable<any> {
    return this.userService.deleteOne(Number(param));
  }

  @UseGuards(JwtAuthGuard, UserIsUser)
  @Put(':id')
  updateOne(@Param('id') param: string, @Body() user: IUser): Observable<any> {
    return this.userService.updateOne(Number(param), user);
  }

  @hasRole(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RoleGuard)
  @Put(':id/role')
  updateRoleOfUser(
    @Param('id') id: string,
    @Body() user: IUser,
  ): Observable<IUser> {
    return this.userService.updateRoleOfUser(Number(id), user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('upload')
  @UseInterceptors(FileInterceptor('file', storage))
  uploadFile(@UploadedFile() file, @Request() req): Observable<any> {
    const user: IUser = req.user;
    return this.userService
      .updateOne(user.id, { profileImage: file.filename })
      .pipe(
        tap((user: IUser) => {
          console.log(user);
        }),
        map((user: IUser) => ({ profileImage: user.profileImage })),
      );
  }

  @Get('profile-image/:imagename')
  findProfileImage(@Param('imagename') imagename, @Res() res): Observable<any> {
    return of(
      res.sendFile(join(process.cwd(), 'uploads/profileimages/' + imagename)),
    );
  }
}
